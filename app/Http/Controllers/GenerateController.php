<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Datasource;
use App\DetailDatasource;
use App\DetailDatasourceDetail;
use App\Template;
use DB;
use PDO;
use App;
use ZipArchive;

class GenerateController extends Controller
{
    //
    public function index(){
        $templateAll = Template::get();
        return view('generate',compact('templateAll'));
    }

    public function templateSelected(Request $r){
        $templateAll = Template::get();
        $template = Template::where("template_id",$r->template_id)->first();

        //show column structure
        $datasourceSelected = Datasource::where("datasource_id",$template->datasource_id)->first();
        $detailDatasource = DetailDatasource::where("datasource_id",$template->datasource_id)->get();
        $tableColumns = DB::select( DB::raw("SHOW COLUMNS FROM ".$datasourceSelected->table_name));

        $selectedTable = DB::table($datasourceSelected->table_name);
        
        //select list
        foreach($detailDatasource as $detail){
            $selectedTable->selectRaw($detail->field_name);
        }

        $selectedTable = $selectedTable->get()->toArray();

        // $selectedTable = array_values($selectedTable);
        // return $selectedTable;

        return view('generate',compact('templateAll','template','selectedTable','detailDatasource','tableColumns'));    

    }

    public function filter(Request $r){
        $templateAll = Template::get();
        $template = Template::where("template_id",$r->template_id)->first();

        //show column structure
        $datasourceSelected = Datasource::where("datasource_id",$template->datasource_id)->first();
        $detailDatasource = DetailDatasource::where("datasource_id",$template->datasource_id)->get();
        $tableColumns = DB::select( DB::raw("SHOW COLUMNS FROM ".$datasourceSelected->table_name));

        $selectedTable = DB::table($datasourceSelected->table_name);
        //select list
        foreach($detailDatasource as $detail){
            $selectedTable->selectRaw($detail->field_name);
            foreach($tableColumns as $column){
                if($detail->field_name == $column->Field){
                    if( // Jika Numerik
                        stripos($column->Type, 'int') !== false ||
                        stripos($column->Type, 'double') !== false ||
                        stripos($column->Type, 'float') !== false
                    ){
                        if($r->{$column->Field."_minimum"} == ""){
                            $r->{$column->Field."_minimum"} = 0;
                        }

                        if($r->{$column->Field."_maximum"} == ""){
                            $r->{$column->Field."_maximum"} = 0;
                        }

                        if($r->{$column->Field."_maximum"} == "" && $r->{$column->Field."_minimum"} != ""){
                            $r->{$column->Field."_maximum"} = $r->{$column->Field."_minimum"}+99999999999999;
                        }

                        if($r->{$column->Field."_minimum"} != "" && $r->{$column->Field."_maximum"} != ""){
                            $selectedTable->whereRaw($detail->field_name." BETWEEN ".$r->{$column->Field."_minimum"}." AND ".$r->{$column->Field."_maximum"});
                        }                        
                    }else{ //Jika String
                        if($r->{$column->Field} != ""){
                            $selectedTable->whereRaw($detail->field_name." LIKE '%".$r->{$column->Field}."%'");
                        }
                    }
                }
            }
        }

        //filter list
        // foreach($detailDatasource as $detail){
            
        // }

        $selectedTable = $selectedTable->get()->toArray();

        // $selectedTable = array_values($selectedTable);
        // return $selectedTable;

        return view('generate',compact('templateAll','template','selectedTable','detailDatasource','tableColumns'));  
    }

    public function generatePdf(Request $r){
        $templateAll = Template::get();
        $template = Template::where("template_id",$r->template_id)->first();

        $datasourceSelected = Datasource::where("datasource_id",$template->datasource_id)->first();
        $detailDatasource = DetailDatasource::where("datasource_id",$template->datasource_id)->get();

        if($datasourceSelected->table_detail_name != '0'){ //Jika ada tabel detail
            $detailDatasourceDetail = DetailDatasourceDetail::where("datasource_id",$template->datasource_id)->get();
        }

        $selectedTable = DB::table($datasourceSelected->table_name);
        $selectedTable->selectRaw($datasourceSelected->fk_field_name);
        foreach($detailDatasource as $detail){
            $selectedTable->selectRaw($detail->field_name);
        }

        $selectedTable = $selectedTable->get();

        $i = 0; $a = 0; $firstColumnName; $j = 0;
        $allPdfString = "";
        foreach($selectedTable as $dataTable){

            $stringToPdf = $template->konten;

            //detail

            if(isset($r->col[$i])){
                if($r->col[$i] == 1){

                    foreach($detailDatasource as $column){

                        if($column->option == 1){
                            $dataTable->{$column->field_name} = $this->format1($dataTable->{$column->field_name});
                        }
                        if($column->option == 2){
                            $dataTable->{$column->field_name} = $this->format2($dataTable->{$column->field_name});
                        }
                        if($column->option == 3){
                            $dataTable->{$column->field_name} = $this->format3($dataTable->{$column->field_name});
                        }
                        if($column->option == 4){
                            $dataTable->{$column->field_name} = $this->format4($dataTable->{$column->field_name});
                        }
                        if($column->option == 5){
                            $dataTable->{$column->field_name} = $this->format5($dataTable->{$column->field_name});
                        }

                        $stringToPdf = str_replace("[[".$column->field_name."]]",$dataTable->{$column->field_name},$stringToPdf);


                        if($datasourceSelected->table_detail_name != '0'){
                                    $selectedTableDetail = DB::table($datasourceSelected->table_detail_name);
                                    foreach($detailDatasourceDetail as $detail){
                                        $selectedTableDetail->selectRaw($detail->field_name);
                                    }
                                    $selectedTableDetail->where($datasourceSelected->fk_field_name,$dataTable->{$datasourceSelected->fk_field_name});
                                    $selectedTableDetail = $selectedTableDetail->get();
        
                                    foreach($detailDatasourceDetail as $detail){

                                        $stringDetail = "<ul>";
                                        foreach($selectedTableDetail as $tabelDetail){
                                            $stringDetail .= "<li>".$tabelDetail->{$detail->field_name}."</li>";
                                        }
                                        $stringDetail .="</ul>"; 
        
                                    }
                                    $stringToPdf = str_replace("[[".$detail->field_name."]]",$stringDetail,$stringToPdf);
                        }

                        $a++;
                    }

                    $allPdfString .= $stringToPdf;
                    //page break
                    $allPdfString .= "<div style='page-break-before: always;'></div>";
    
                }
            }

            $i++;
        }

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML($allPdfString);
        $pdf->stream();
        return $pdf->download("generated.pdf");

        // $zip->close();
        // header('Content-Type: application/zip');
        // header("Content-Disposition: attachment; filename='".$zipname."'");
        // header("Pragma: no-cache"); 
        // header("Expires: 0");
        // header('Cache-Control: must-revalidate');
        // header('Content-Length: ' . filesize($zipname));

        return view('generate',compact('templateAll','template','selectedTable','detailDatasource'));
    }

    function format2($angka){
	
        $hasil_rupiah = "Rp " . number_format($angka,2,',','.').'-';
        return $hasil_rupiah;
     
    }  
    
    function format1($angka){
	
        $hasil_rupiah = number_format($angka,0,',','.');
        return $hasil_rupiah;
     
    }  

    function format3($angka){
	
        $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
        return $hasil_rupiah;
     
    }  

    function format4($angka){

        $hasilTerbilang = $this->terbilang($angka);
	
        $hasil_rupiah = "Rp " . number_format($angka,2,',','.').'-'.' '.$hasilTerbilang;
        return $hasil_rupiah;
     
    }  

    function format5($angka){

        $hasilTerbilang = $this->terbilang($angka);
	
        $hasil_rupiah = "Rp " . number_format($angka,0,',','.').' '.$hasilTerbilang;
        return $hasil_rupiah;
     
    } 

    function penyebut($nilai) {
		$nilai = abs($nilai);
		$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = $this->penyebut($nilai - 10). " belas";
		} else if ($nilai < 100) {
			$temp = $this->penyebut($nilai/10)." puluh". $this->penyebut($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " seratus" . $this->penyebut($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = $this->penyebut($nilai/100) . " ratus" . $this->penyebut($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " seribu" . $this->penyebut($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = $this->penyebut($nilai/1000) . " ribu" . $this->penyebut($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = $this->penyebut($nilai/1000000) . " juta" . $this->penyebut($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = $this->penyebut($nilai/1000000000) . " milyar" . $this->penyebut(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = $this->penyebut($nilai/1000000000000) . " trilyun" . $this->penyebut(fmod($nilai,1000000000000));
		}     
		return $temp;
	}
 
	function terbilang($nilai) {
		if($nilai<0) {
			$hasil = "minus ". trim($this->penyebut($nilai));
		} else {
			$hasil = trim($this->penyebut($nilai));
		}     		
		return $hasil;
	}

}
