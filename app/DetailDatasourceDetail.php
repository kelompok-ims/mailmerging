<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailDatasourceDetail extends Model
{
    //
    protected $table = 'tb_detail_datasource_detail';
    protected $primaryKey = 'detail_datasource_detail_id';
    public $timestamps = false;
}
